[![pipeline status](https://gitlab.com/synack3/shuffle_dev/badges/master/pipeline.svg)](https://gitlab.com/synack3/shuffle_dev/-/commits/master) 
# shuffle_dev

This is a personal repository for exploring various aspects of the Shuffle Security Orchestration, Automation, and Response platform (https://shuffler.io).  It is made generally available for anyone who is interested in performing similar explorations.

# Structure

The overall structure of this project is: 

- apis/ - API specifications using the swagger standard.  Ideally, these would be able to imported to Shuffle via the Apps interface using the `Create from OpenAPI` button. 
- apps/ - Custom apps to extend Shuffle's "plug-and-play" architecture.  These apps should be able to be dropped into Shuffle's shuffle-apps subdirectory and loaded via the Apps interface using the `Reload Apps Locally` button.
- utils/ - Random utilities meant to expedite or simplify workflows with Shuffle.
- workflows/ - JSON files representing specific Shuffle workflows.  These should be able to be imported into Shuffle via the Workflows interface using the `Create new workflow` button.

