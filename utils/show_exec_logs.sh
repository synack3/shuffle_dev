#!/bin/bash

execution=`docker logs shuffle-backend 2>&1 | egrep "with execution environment" | tail -n 1 | awk '{ print($5) }'`
workflow_container=`docker logs shuffle-orborus 2>&1 | egrep "was created under" | tail -n 1 | awk '{ print($5) }'`
node_container=`docker logs $workflow_container 2>&1 | egrep Container | egrep -v "CREATE" | awk '{ print($5); }'`

echo Execution ID: $execution
echo Workflow Container: $workflow_container
echo Node Containers: $node_container

for node in $node_container
do
    echo -------- BEGIN NODE $node LOGS -------
    docker logs $node
    echo -------- END NODE $node LOGS -------
done