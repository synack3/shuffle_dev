# Deprecation Notice

The Shuffle datetools app has been deprecated.  The functionality created here has been 
incorporated into Shuffle core as part of the Shuffle Tools app.  The app will remain in 
this repository for historical reference.